from django.shortcuts import render
import qrcode
# Create your views here.
from rest_framework import viewsets , permissions , serializers
from url_filter.integrations.drf import DjangoFilterBackend
from .serializers import *
from .serializers import *
from apimanagement.permissions import *
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse
from datetime import datetime, timedelta, date
import json
from .models import *
from django.template.loader import render_to_string, get_template
from django.core.mail import send_mail, EmailMessage
from django.db.models import F ,Value,CharField,Q
from django.contrib.auth.models import User , Group
from django.shortcuts import render, redirect , get_object_or_404
from django.contrib.auth import authenticate , login , logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.mail import send_mail, EmailMessage
from django.urls import reverse
from django.template import RequestContext
from django.conf import settings as globalSettings
from django.core.exceptions import ObjectDoesNotExist , SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from rest_framework import viewsets , permissions , serializers
from rest_framework.exceptions import *
from django.db.models import Q
from django.http import JsonResponse
from django.db.models import Avg, Count, Min, Sum
from django.utils.crypto import get_random_string
import os
import random
import string
import requests
import razorpay
import datetime
from django.conf import settings as globalSettings


class TableViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = TableSerializer
    filter_fields = []
    def get_queryset(self):
        params=self.request.GET
        toReturn = Table.objects.all()
        return toReturn
class CategoryViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = CategorySerializer
    filter_fields = []
    permission_classes = (permissions.AllowAny , )
    def get_queryset(self):
        params=self.request.GET
        toReturn = Category.objects.all()
        return toReturn
class DrollsExpensesViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = DrollsExpensesSerializer
    filter_fields = []
    def get_queryset(self):
        params=self.request.GET
        toReturn = DrollsExpenses.objects.all()
        return toReturn
class IngridientViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = IngridientSerializer
    filter_fields = []
    def get_queryset(self):
        params=self.request.GET
        toReturn = Ingridient.objects.all()
        if 'search' in params:
            toReturn = toReturn.filter(title__icontains=params['search'])
            if toReturn.count() > 6:
                toReturn = toReturn[:5]
                return toReturn
            return toReturn

        return toReturn
class InventoryCategoryViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = InventoryCategorySerializer
    filter_fields = []
    def get_queryset(self):
        params=self.request.GET
        toReturn = InventoryCategory.objects.all()
        return toReturn
class ItemSubViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = ItemSubSerializer
    filter_fields = ['status']
    def get_queryset(self):
        params=self.request.GET
        toReturn = ItemSub.objects.all()
        return toReturn
class ItemViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = ItemSerializer
    filter_fields = ['category']
    permission_classes = (permissions.AllowAny , )
    def get_queryset(self):
        params=self.request.GET
        toReturn = Item.objects.all()
        if 'price' in params:
            toReturn = toReturn.filter(item_price__lte=params['price'])
            return toReturn
        if 'search' in params:
            toReturn = toReturn.filter(title__icontains=params['search'])
            if toReturn.count() > 6:
                toReturn = toReturn[:5]
                return toReturn
            return toReturn

        return toReturn
class CartItemViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = CartItemSerializer
    filter_fields = []
    def get_queryset(self):
        params=self.request.GET
        toReturn = CartItem.objects.all()
        return toReturn


class CartViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = CartSerializer
    filter_fields = ['order_type','cart_status']

    def get_queryset(self):
        params=self.request.GET
        toReturn = Cart.objects.all()
        if 'date' in params:
            toReturn = toReturn.filter(created__istartswith=str(params['date']))
            return toReturn
        return toReturn

class OrderViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = OrderSerializer
    filter_fields = ['order_status']
    def get_queryset(self):
        params=self.request.GET
        toReturn = Order.objects.all()
        return toReturn
class IngridientSubViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    serializer_class = IngridientSubSerializer
    filter_fields = ['main','status']
    def get_queryset(self):
        params=self.request.GET
        toReturn = IngridientSub.objects.all()
        return toReturn






class create_cart(APIView):
    def post(self,request,format=None):
        data = request.data
        if 'add_cart' in data:
            if  data['tablepk']:
                table = Table.objects.get(pk=data['tablepk'])
                cart = Cart.objects.create(table=table)
                for item in data['items']:
                    item_obj = Item.objects.get(pk=int(item['id']))
                    cart_item = CartItem.objects.create(item=item_obj,quantity=item['quantity'],comments=item['comments'],item_price=item_obj.item_price,discount_price=item_obj.discount_price*item['quantity'],total_price=item_obj.item_price*item['quantity'] - item_obj.discount_price*item['quantity'])
                    cart.items.add(cart_item)
                cart.total_price =  cart.items.all().aggregate(sum=Sum('item_price'))['sum'] or 0
                cart.money_saved =  cart.items.all().aggregate(sum=Sum('discount_price'))['sum'] or 0
                cart.cart_bill =  cart.total_price - cart.money_saved
                cart.save()
                table.active = True
                table.save()
                return Response({"success":"cart created succesfully","cartpk":cart.pk},status=201)
            if  data['user']:
                user = request.user
                cart = Cart.objects.create(user=user)
                for item in data['items']:
                    item_obj = Item.objects.get(pk=int(item['id']))
                    cart_item = CartItem.objects.create(item=item_obj,quantity=item['quantity'],comments=item['comments'],item_price=item_obj.item_price,discount_price=item_obj.discount_price*item['quantity'],total_price=item_obj.item_price*item['quantity'] - item_obj.discount_price*item['quantity'])
                    cart.items.add(cart_item)
                cart.total_price =  cart.items.all().aggregate(sum=Sum('item_price'))['sum'] or 0
                cart.money_saved =  cart.items.all().aggregate(sum=Sum('discount_price'))['sum'] or 0
                cart.order_type = data['type']
                cart.cart_bill =  cart.total_price - cart.money_saved
                cart.save()
                return Response({"success":"cart created succesfully","cartpk":cart.pk},status=201)
        if 'edit_cart' in data:
            cart = Cart.objects.get(id=int(data['cart']))
            for item in data['items']:
                item_obj = Item.objects.get(pk=int(item['id']))
                cart_item = CartItem.objects.create(item=item_obj,quantity=item['quantity'],comments=item['comments'],item_price=item_obj.item_price,discount_price=item_obj.discount_price*item['quantity'],total_price=item_obj.item_price*item['quantity'] - item_obj.discount_price*item['quantity'])
                cart.items.add(cart_item)
            cart.total_price =  cart.items.all().aggregate(sum=Sum('item_price'))['sum'] or 0
            cart.money_saved =  cart.items.all().aggregate(sum=Sum('discount_price'))['sum'] or 0
            cart.cart_bill =  cart.total_price - cart.money_saved
            cart.save()
            return Response({"success":"cart edited succesfully","cartpk":cart.pk},status=201)

        return Response({"failed":"required credientals are not specified"})


from calendar import monthrange
class post_expenses(APIView):
    def post(self,request,format=None):
        data = request.data
        expense_obj = DrollsExpenses.objects.all().exists()
        if expense_obj:
            expense_obj = DrollsExpenses.objects.first()
            if 'employee_expense' in data:
                expense_obj.employee_expense = data['employee_expense']
            if 'other_expense' in data:
                expense_obj.other_expense = data['other_expense']
            if 'electricity_expense' in data:
                expense_obj.electricity_expense = data['electricity_expense']
            expense_obj.total_expense = expense_obj.employee_expense + expense_obj.other_expense + expense_obj.electricity_expense
            expense_obj.save()
            return Response({'success':"expenses updated"},status=201)
        if not expense_obj:
            expense_obj = DrollsExpenses()
            if 'employee_expense' in data:
                expense_obj.employee_expense = data['employee_expense']
            if 'other_expense' in data:
                expense_obj.other_expense = data['other_expense']
            if 'electricity_expense' in data:
                expense_obj.electricity_expense = data['electricity_expense']
            expense_obj.total_expense = expense_obj.employee_expense + expense_obj.other_expense + expense_obj.electricity_expense
            expense_obj.save()
            return Response({"success":"expense created succesfully"},status=201)



class cart_completed(APIView):
    def post(self,request,format=None):
        data = request.data
        cart = Cart.objects.prefetch_related('items').get(id=int(data['cart_id']))
        cart_items = cart.items.all()
        if data['status'] == 'Completed' or data['status'] == 'PreparedNDeclined':
            for item in cart_items:
                ingridients = item.item.ingridients.all()
                for ing in ingridients:
                    total_qty = ing.ingridient.ingridientsubs.all().aggregate(sum=Sum('quantity'))['sum'] or 0
                    if total_qty < ing.grams:
                        return Response({"failed":"required quantity is not available"},status=500)
                    if total_qty > ing.grams:
                        required_grams = ing.grams
                        print(required_grams,"heree")
                        while required_grams > 0:
                            print("heree")
                            current_obj = ing.ingridient.ingridientsubs.all().order_by('-quantity')[0]
                            if current_obj.quantity > required_grams:
                                per_quantity = current_obj.price // current_obj.quantity
                                current_obj.quantity -= required_grams
                                current_obj.price -= required_grams * per_quantity
                                current_obj.save()
                                new_obj = current_obj
                                new_obj.pk = None
                                new_obj.quantity = required_grams
                                new_obj.price = required_grams * per_quantity
                                new_obj.save()
                                required_grams = 0
                            if current_obj.quantity == required_grams:
                                current_obj.status = 'Sold'
                                current_obj.save()
                                required_grams = 0
                            if current_obj.quantity < required_grams:
                                required_grams -= current_obj.quantity
                                current_obj.status = 'Sold'
                                current_obj.save()
            cart.gst = (cart.cart_bill * 8) // 100
            cart.cart_bill += cart.gst
            cart.cart_status = data['status']
            cart.payment_status=data['payment_status']
            cart.save()
            return Response({"success":"order created"},status=201)
        if data['status'] == 'Pending' or data['status'] == 'Declined':
            cart.cart_status = data['status']
            cart.save()
            return Response({"success":"order created"},status=201)
        return Response({"failed":"please enter the required credientals"},status=500)








class create_purchase_order(APIView):
    def get(self,request,format=None):
        items = Ingridient.objects.prefetch_related('ingridientsubs').all()
        toSend = []
        for item in items:
            total_qty = item.ingridientsubs.all().aggregate(sum=Sum('quantity'))['sum'] or 0
            if total_qty < item.minimum_quantity:
                dict = {
                    'title':item.title,
                    'ingridient':item.pk,
                    'quantity':0,
                }
                dict['quantity'] = item.minimum_quantity - total_qty
                toSend.append(dict)
        return JsonResponse(toSend,safe=False)

class cook_view(APIView):
    def get(self,request,format=None):
        pending_items = {}
        active_carts = Cart.objects.filter(cart_status="Pending").order_by('created')
        for cart in active_carts:
            cart_items = cart.items.all()
            for item in cart_items:
                if str(item.item.pk) in pending_items:
                    c = {"status":item.item_status,"order_for":cart.order_type,'quantity':item.quantity,'pk':item.pk,'table':cart.table.id if cart.table is not None else "None" ,'time':item.created}
                    pending_items[str(item.item.pk)]['objs'].append(c)
                    pending_items[str(item.item.pk)]['itemcount'] +=item.quantity
                if str(item.item.pk) not in pending_items:
                    pending_items[str(item.item.pk)] = {
                        "title":item.item.title,
                        'itemcount':item.quantity,
                        'objs':[],
                        }
                    c = {"status":item.item_status,"order_for":cart.order_type,'pk':item.pk,'quantity':item.quantity,'table':cart.table.id if cart.table is not None else "None" ,'time':item.created}
                    pending_items[str(item.item.pk)]['objs'].append(c)
        pending_items = [pending_items[item] for item in pending_items]
        return JsonResponse(pending_items,safe=False)

class cook_order_complete(APIView):
    def post(self,request,format=None):
        data = request.data
        if 'items' and 'status' in data:
            CartItem.objects.filter(id__in=data['items']).update(item_status=data['status'])
            return Response({"success":"status changed succesfully"},status=201)
        return Response({"failed":"please enter the required params"})








class create_order(APIView):
    def post(self,request,format=None):
        data = request.data
        order = Order.objects.create(arriving_date=data['date'])
        for item in data['items']:
            main_ing = Ingridient.objects.get(pk=int(item['ingridient']))
            obj = IngridientSub.objects.create(main=main_ing,quantity=item['quantity'],status='Pending')
            order.items.add(obj)
        order.save()
        return Response({"success":"order created succesfully"},status=201)


class net_profit(APIView):
    def get(self,request,format=None):
        params = request.GET
        required = ['Sold','Loss','InStock']
        if int(params['month'])==0:
            month_list=['ALLMONTH','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            data = []
            for i in range(1,13):
                # name = names[str(i)]

                start_date = datetime.datetime(int(params['year']),i,1).date()
                days = monthrange(int(params['year']),int(i))[1]
                end_date = start_date + timedelta(days=int(days))
                orders_amount = Cart.objects.filter(created__range=[start_date,end_date],cart_status='Completed').aggregate(sum=Sum('cart_bill'))['sum'] or 0
                ingridient_amount = IngridientSub.objects.filter(created__range=[start_date,end_date],status__in=required).aggregate(sum=Sum('price'))['sum'] or 0
                other_expenses = DrollsExpenses.objects.first()
                print(ingridient_amount)
                data.append(orders_amount-(ingridient_amount+other_expenses.total_expense))
            return JsonResponse({"labels":month_list[1:],"data":data})
        if int(params['month'])!=0:
            names = ['First Week','Second Week','Third Week','Fourth Week','Fifth Week']
            data = []
            month_num = int(params['month'])
            # name = names[str(i)]
            days = monthrange(int(params['year']),int(month_num))[1]
            remaining_days = days % 7
            week_days = [7,7,7,7]
            if remaining_days != 0:
                week_days.append(remaining_days)
            toReturn = []
            for indx,i in enumerate(week_days):
                if indx == 0:
                    start_date = datetime.datetime(int(params['year']),month_num,1).date()
                else:
                    days_count = indx * 7
                    start_date = datetime.datetime(int(params['year']),month_num,days_count)
                end_date = start_date + timedelta(days=i)
                orders_amount = Cart.objects.filter(created__range=[start_date,end_date],cart_status='Completed').aggregate(sum=Sum('cart_bill'))['sum'] or 0
                ingridient_amount = IngridientSub.objects.filter(created__range=[start_date,end_date],status__in=required).aggregate(sum=Sum('price'))['sum'] or 0
                other_expenses = DrollsExpenses.objects.first()
                other_expenses = other_expenses.total_expense//len(week_days) if other_expenses.total_expense > 0 else 0
                data.append(orders_amount-(ingridient_amount+other_expenses))
            return JsonResponse({"labels":names,"data":data},safe=False)


class peak_time(APIView):
    def get(self,request,format=None):
        params = request.GET
        if int(params['month']) == 0:
                start_date = datetime.datetime(int(params['year']),1,1).date()
                end_date = datetime.datetime((int(params['year'])+1),1,1).date()
        else:
            i = int(params['month'])
            # name = names[str(i)]
            start_date = datetime.datetime(int(params['year']),i,1).date()
            days = monthrange(int(params['year']),int(i))[1]
            end_date = start_date + timedelta(days=int(days))
            toReturn = []
        data = []
        time_array = ['12 AM to 1 AM','1 AM to 2 AM','2 AM to 3 AM','3 AM to 4 AM','4 AM to 5 AM','5 AM to 6 AM','6 AM to 7 AM','7 AM to 8 AM','8 AM to 9 AM','9 AM to 10 AM','10 AM to 11 AM','11 AM to 12 PM','12 PM to 1 PM','1 PM to 2 PM','2 PM to 3 PM','3 PM to 4 PM','4 PM to 5 PM','5 PM to 6 PM','6 PM to 7 PM','7 PM to 8 PM','8 PM to 9 PM','9 PM to 10 PM','10 PM to 11 PM','11 PM to 12 AM']
        for i in range(0,24):
            total_orders = Cart.objects.filter(created__hour=i).filter(created__range=[start_date,end_date]).count()
            data.append(total_orders)
        return JsonResponse({"labels":time_array,'data':data},safe=False)







class add_items(APIView):
    def post(self,request,format=None):
        data = request.data
        print(data)
        item_obj = Item()
        item_obj.displayPicture = data['displayPicture']
        item_obj.item_price = data['item_price']
        item_obj.discount_price = data['discount_price']
        item_obj.title = data['title']
        item_obj.description = data['description']
        item_obj.category = Category.objects.get(pk=int(data['category']))
        item_obj.save()
        return Response({"success":"item created succesfully","itempk":item_obj.id})

class ingridient_add(APIView):
    def post(self,request,format=None):
        data = request.data
        item_obj = Item.objects.get(id=int(data['pk']))
        main_ing = Ingridient.objects.get(pk=int(data['ingridient']))
        obj = ItemSub.objects.create(ingridient=main_ing,grams=data['grams'])
        item_obj.ingridients.add(obj)
        item_obj.save()
        return Response({"success":"Ingridents added Succesfully"},status=201)

class fast_moving(APIView):
    def get(self,request,format=None):
        data = request.GET
        if 'start' and 'end' in data:
            start = data['start']
            end = data['end']
            orders = Cart.objects.filter(created__range=[start,end],cart_status='Completed')
            items = Item.objects.all()
            names = [item.title for item in items]
            moving = [orders.filter(items__item=item).count() for item in items]
            if len(names) > 20:
                names=names[:20]
                moving = moving[:20]
            return JsonResponse({"labels":names,"data":moving},safe=False)
        return Response({"failed":"please enter the required params"},status=400)







from django.core.files import File
class create_table(APIView):
    def post(self,request,format=None):
        data = request.data
        table = Table()
        table.total_persons =  data['total_persons']
        table.save()
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        qr.add_data(f'http://192.168.29.98:9000/{table.id}')
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")
        path = f'{globalSettings.MEDIA_ROOT}/images/qrcode{table.id}.png'
        img.save(path)
        f = open(path,'rb')
        my_file = File(f)
        table.qr_code = my_file
        table.save()
        os.remove(path)
        return Response({'success':"table created succesfully"})
