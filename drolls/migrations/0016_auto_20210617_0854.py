# Generated by Django 3.0.5 on 2021-06-17 08:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drolls', '0015_auto_20210617_0656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ingridientsub',
            name='status',
            field=models.CharField(blank=True, choices=[('Sold', 'Sold'), ('Loss', 'Loss'), ('Cancelled', 'Cancelled'), ('Pending', 'Pending'), ('InStock', 'InStock')], default='InStock', max_length=500, null=True),
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('arriving_date', models.DateField(blank=True, null=True)),
                ('order_status', models.CharField(choices=[('Completed', 'Completed'), ('Pending', 'Pending'), ('Cancelled', 'Cancelled')], default='Pending', max_length=250, null=True)),
                ('amount', models.FloatField(default=0)),
                ('discount', models.FloatField(default=0)),
                ('items', models.ManyToManyField(blank=True, related_name='ordereditems', to='drolls.ItemSub')),
            ],
        ),
    ]
