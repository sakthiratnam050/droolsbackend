# Generated by Django 3.0.5 on 2021-06-16 08:14

from django.db import migrations, models
import drolls.models


class Migration(migrations.Migration):

    dependencies = [
        ('drolls', '0005_auto_20210615_1523'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartitem',
            name='comments',
            field=models.TextField(default=0),
        ),
        migrations.AddField(
            model_name='cartitem',
            name='quantity',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='table',
            name='qr_code',
            field=models.ImageField(null=True, upload_to=drolls.models.getQrPath),
        ),
    ]
