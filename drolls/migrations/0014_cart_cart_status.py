# Generated by Django 3.0.5 on 2021-06-17 06:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drolls', '0013_auto_20210617_0638'),
    ]

    operations = [
        migrations.AddField(
            model_name='cart',
            name='cart_status',
            field=models.CharField(choices=[('Completed', 'Completed'), ('Declined', 'Declined'), ('PreparedNDeclined', 'PreparedNDeclined'), ('Pending', 'Pending')], default='Pending', max_length=250, null=True),
        ),
    ]
