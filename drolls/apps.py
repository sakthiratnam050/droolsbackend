from django.apps import AppConfig


class DrollsConfig(AppConfig):
    name = 'drolls'
