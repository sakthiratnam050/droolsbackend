from django.db import models
from django.contrib.auth.models import User
from time import time
# Create your models here.

def getItemPath(instance , filename):
    toReturn = 'images/Items/%s_%s_%s' % (str(time()).replace('.', '_'), instance.title, filename)
    return toReturn


def getQrPath(instance , filename):
    toReturn = 'images/Qrs/%s_%s_%s' % (str(time()).replace('.', '_'), instance.id, filename)
    return toReturn
# class accountsKey(models.Model):
#     user = models.ForeignKey(User , related_name='accountKey',on_delete=models.CASCADE)
#     activation_key = models.CharField(max_length=40, blank=True)
#     key_expires = models.DateTimeField(default=timezone.now)
#     keyType = models.CharField(max_length = 15 , default = 'hashed' , choices = KEY_CHOICES)

order_choices = (
    ('Pending', 'Pending'),
    ('Completed', 'Completed'),
    ('Declined', 'Declined'),
)
order_types = (
    ('Dining', 'Dining'),
    ('Takeaway', 'Takeaway'),
    ('Online', 'Online'),
)
payment_choices = (
    ('Paid', 'Paid'),
    ('NotPaid', 'NotPaid'),
)



class Table(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    total_persons = models.PositiveIntegerField(default=0)
    active = models.BooleanField(default=False)
    qr_code = models.ImageField(upload_to = getQrPath, null = True)



class Category(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=500)

ingrident_types = (
    ('Piece', 'Piece'),
    ('Gram', 'Gram'),
)

class Ingridient(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=500,null=True,blank=True)
    description = models.TextField(max_length=500,null=True,blank=True)
    minimum_quantity = models.FloatField(default=0)
    type = models.CharField(null = True, max_length = 250 , choices = ingrident_types ,default='Piece')

class InventoryCategory(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    grams = models.FloatField(default=0)
    discount = models.FloatField(default=0)
    price = models.FloatField(default=0)
    status = models.CharField(max_length=500,null=True,blank=True,)
    expiry_date = models.DateField(null=True,blank=True)

sub_choices = (
    ('Sold', 'Sold'),
    ('Loss', 'Loss'),
    ('Cancelled', 'Cancelled'),
    ('Pending', 'Pending'),
    ('InStock', 'InStock'),
)
purchase_order = (
    ('Completed', 'Completed'),
    ('Pending', 'Pending'),
    ('Cancelled', 'Cancelled'),

)

class IngridientSub(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    quantity = models.FloatField(default=0)
    # discount = models.FloatField(default=0)
    main = models.ForeignKey(Ingridient,null=True,blank=True,related_name="ingridientsubs",on_delete=models.CASCADE)
    price = models.FloatField(default=0)
    status = models.CharField(max_length=500,null=True,blank=True,default='InStock',choices = sub_choices)
    expiry_date = models.DateField(null=True,blank=True)

class ItemSub(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    ingridient = models.ForeignKey(Ingridient,null=True,blank=True,on_delete=models.CASCADE)
    grams = models.FloatField(default=0)

class Order(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    arriving_date = models.DateField(null=True,blank=True)
    order_status = models.CharField(null = True, max_length = 250 , choices = purchase_order ,default='Pending')
    amount = models.FloatField(default=0)
    discount = models.FloatField(default=0)
    items = models.ManyToManyField(IngridientSub,blank=True,related_name="ordereditems")

# class ItemCategory(models.Model):
#     created = models.DateTimeField(auto_now_add=True)
#     updated = models.DateTimeField(auto_now=True)
#     title = models.CharField(null=True,blank=True,max_length=500)
class Item(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    displayPicture = models.ImageField(upload_to = getItemPath, null = True)
    category = models.ForeignKey(Category,related_name="categoryitems",null=True,blank=True,on_delete=models.CASCADE)
    item_price = models.FloatField(default=0)
    discount_price = models.FloatField(default=0)
    title = models.CharField(null=True,blank=True,max_length=500)
    ingridients = models.ManyToManyField(ItemSub,blank=True,related_name="useditems")
    description = models.TextField(null=True,blank=True,max_length=5000)

cartitem_status = (
('Cooking', 'Cooking'),
('Pending', 'Pending'),
('Finished', 'Finished'),
('Declined', 'Declined'),
)
class CartItem(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    item = models.ForeignKey(Item,related_name='cartitems',on_delete=models.CASCADE)
    item_status = models.CharField(null = True, max_length = 250 , choices = cartitem_status ,default='Pending')
    item_price = models.FloatField(default=0)
    finished_quantity = models.PositiveIntegerField(default=0)
    quantity = models.PositiveIntegerField(default=0)
    comments = models.TextField(default=0)
    is_finished = models.BooleanField(default=False)
    discount_price = models.FloatField(default=0)
    total_price = models.FloatField(default=0)

order_status = (
('Completed', 'Completed'),
('Declined', 'Declined'),
('PreparedNDeclined', 'PreparedNDeclined'),
('Pending', 'Pending'),
)

class Cart(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    items = models.ManyToManyField(CartItem,related_name="cart",blank=True)
    total_price = models.FloatField(default=0)
    money_saved = models.FloatField(default=0)
    active = models.BooleanField(default=True)
    user = models.ForeignKey(User,related_name="totalorders",on_delete=models.CASCADE,blank=True,null=True)
    table = models.ForeignKey(Table,related_name="tableorders",on_delete=models.CASCADE,blank=True,null=True)
    cart_bill = models.FloatField(default=0)
    cart_status = models.CharField(null = True, max_length = 250 , choices = order_status ,default='Pending')
    order_type = models.CharField(null = True, max_length = 250 , choices = order_types ,default='Dining')
    payment_status = models.CharField(null = True, max_length = 250 , choices = payment_choices ,default='Paid')
    gst = models.FloatField(default=0)

class DrollsExpenses(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    employee_expense = models.PositiveIntegerField(default=0)
    other_expense = models.PositiveIntegerField(default=0)
    electricity_expense = models.PositiveIntegerField(default=0)
    total_expense = models.PositiveIntegerField(default=0)
