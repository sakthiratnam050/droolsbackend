from django.contrib.auth.models import User , Group
from django.contrib.auth import authenticate
from rest_framework import serializers
from rest_framework.exceptions import *
from .models import *
from rest_framework.response import Response
from django.utils.timesince import timesince
from datetime import  tzinfo
import datetime
from django.contrib.auth.models import User , Group
from django.db.models import Avg, Count, Min, Sum


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        fields = '__all__'

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class IngridientSerializer(serializers.ModelSerializer):
    avaliableQty = serializers.SerializerMethodField()
    class Meta:
        model = Ingridient
        fields = '__all__'
    def get_avaliableQty(self,obj):
        toSend = {}
        total_qty = obj.ingridientsubs.all().aggregate(sum=Sum('quantity'))['sum'] or 0
        if obj.type == 'Gram':
            toSend['available_kg'] = (total_qty//1000) if total_qty !=0 else 0
            toSend['available_quantity'] = (total_qty%1000) if total_qty !=0 else 0
            return toSend
        if obj.type == 'Piece':
            toSend['available_quantity'] = total_qty
            toSend['available_kg'] = None
        return toSend


class InventoryCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = InventoryCategory
        fields = '__all__'

class DrollsExpensesSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrollsExpenses
        fields = '__all__'

class ItemSubSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemSub
        fields = '__all__'

class ItemSerializer(serializers.ModelSerializer):
    subItems = serializers.SerializerMethodField()
    class Meta:
        model = Item
        fields = '__all__'
    def get_subItems(self,obj):
        return [{'title':item.ingridient.title,'id':item.id,'quantity':item.grams,'type':item.ingridient.type} for item in obj.ingridients.all()]


class IngridientSubSerializer(serializers.ModelSerializer):
    itemTitle = serializers.SerializerMethodField()
    class Meta:
        model = IngridientSub
        fields = '__all__'
    def get_itemTitle(self,obj):
        if obj.main is not None:
            return obj.main.title

class CartItemSerializer(serializers.ModelSerializer):
    itemTitle = serializers.SerializerMethodField()
    class Meta:
        model = CartItem
        fields = '__all__'
    def get_itemTitle(self,obj):
        if obj.item is not None:
            return obj.item.title

class CartSerializer(serializers.ModelSerializer):
    items = CartItemSerializer(read_only=True,many=True)
    class Meta:
        model = Cart
        fields = '__all__'

class OrderSerializer(serializers.ModelSerializer):
    items = IngridientSubSerializer(read_only=True,many=True)
    class Meta:
        model = Order
        fields = '__all__'
