from django.contrib import admin
from django.urls import path,include
from rest_framework import routers
from .views import *
router = routers.DefaultRouter()
router.register(r'tables' , TableViewSet , basename = 'tables')
router.register(r'category' , CategoryViewSet , basename = 'category')
router.register(r'ingridents' , IngridientViewSet , basename = 'ingridents')
router.register(r'inventorycategory' , InventoryCategoryViewSet , basename = 'inventorycategory')
router.register(r'itemsubs' , ItemSubViewSet , basename = 'itemsubs')
router.register(r'items' , ItemViewSet , basename = 'items')
router.register(r'ingridientsub' , IngridientSubViewSet , basename = 'ingridientsub')
router.register(r'cartitems' , CartItemViewSet , basename = 'cartitems')
router.register(r'orders' , OrderViewSet , basename = 'orders')
router.register(r'cart' , CartViewSet , basename = 'cart')
router.register(r'droolsExpenses' , DrollsExpensesViewSet , basename = 'droolsExpenses')
urlpatterns = [
    path('',include(router.urls)),
    path('addItem/',add_items.as_view()),
    path('ingridient_add/',ingridient_add.as_view()),
    path('tableCreate/',create_table.as_view()),
    path('addCart/',create_cart.as_view()),
    path('createOrder/',cart_completed.as_view()),
    path('createPurchaseOrder/',create_order.as_view()),
    path('showMinimum/',create_purchase_order.as_view()),
    path('cookOrders/',cook_view.as_view()),
    path('cookComplete/',cook_order_complete.as_view()),
    path('fastMoving/',fast_moving.as_view()),
    path('postExpenses/',post_expenses.as_view()),
    path('netProfit/',net_profit.as_view()),
    path('peakTime/',peak_time.as_view()),
    # path('createRep/',CreateMedicalRep.as_view()),
    # path('paymentOrder/',PaymentOrder.as_view()),
    # path('validatePayment/',Validate_Payment.as_view()),
    # path('userRegister/',UserRegister),

]
