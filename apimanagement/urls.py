from django.contrib import admin
from django.urls import path,include

urlpatterns = [
    # path('prescription/', include('prescriptionapp.urls')),
    path('profile/', include('profiles.urls')),
    path('drools/', include('drolls.urls')),
    # path('HR/', include('HR.urls')),
    # path('ERP/', include('ERP.urls')),
    # path('PIM/', include('PIM.urls')),
]
