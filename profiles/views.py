from django.shortcuts import render
from django.contrib.auth import authenticate , login , logout
import django
# Create your views here.
from rest_framework import viewsets , permissions , serializers
from url_filter.integrations.drf import DjangoFilterBackend
from .serializers import *
from .serializers import *
from apimanagement.permissions import *
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse
from datetime import datetime, timedelta, date
import json
from .models import *
from django.template.loader import render_to_string, get_template
from django.core.mail import send_mail, EmailMessage
from django.db.models import F ,Value,CharField,Q
from django.contrib.auth.models import User , Group
from django.shortcuts import render, redirect , get_object_or_404
from django.contrib.auth import authenticate , login , logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.mail import send_mail, EmailMessage
from django.urls import reverse
from django.template import RequestContext
from django.conf import settings as globalSettings
from django.core.exceptions import ObjectDoesNotExist , SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from rest_framework import viewsets , permissions , serializers
from rest_framework.exceptions import *
from django.db.models import Q
from django.http import JsonResponse
from django.db.models import Avg, Count, Min, Sum
from django.utils.crypto import get_random_string
import os
import random
import string
import requests
import razorpay
import datetime
# from prescriptionapp.models import clinic


class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.AllowAny , )
    serializer_class = ProfileSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['user']
    def get_queryset(self):
        params=self.request.GET
        if 'position' in params:
            toReturn = profile.objects.filter(occupation=params['position'])
            return toReturn
        if 'searchusers' in params:
            toReturn = profile.objects.filter(occupation='Customer')
            toReturn = toReturn.filter(Q(user__first_name__icontains=params['searchusers'])|Q(user__username__icontains=params['searchusers']))
            if toReturn.count() > 5:
                toReturn = toReturn[:5]
                return toReturn
            else:
                return toReturn
        if 'search' in params and 'role' in params:
            toReturn = profile.objects.filter(occupation=params['role'])
            toReturn = toReturn.filter(Q(user__first_name__icontains=params['search'])|Q(user__username__icontains=params['search'])|Q(specialization__icontains=params['search']))
            toReturn = toReturn[:5]
            return toReturn
        toReturn = profile.objects.all()
        return toReturn

@csrf_exempt
def loginView(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username = username , password = password)
        if user is not None:
            login(request , user)
            csrf_token = django.middleware.csrf.get_token(request)
            if 'mode' in request.GET and request.GET['mode'] == 'api':
                return JsonResponse({'csrf_token':csrf_token , "pk" : user.pk,'title':user.profile.occupation} , status = 200)
        return Response({"failed":"register and login"},status=500)
    return Response({"failed":"post method only"},status=500)
def logoutView(request):
    logout(request)
    return Response({"success":"user logout successfully"})
class CreateUsers(APIView):
    def post(self,request,format=None):
        data = request.data
        print(data,"awfewaefewf")
        if 'mobile' in data and 'name' in data:
            user,created = User.objects.get_or_create(username=data['mobile'],first_name=data['name'])
            user.set_password(data['mobile'])
            user.save()
            if created:
                print("sdfasdf")
                try:
                    profile = user.profile
                    profile.pan = data['pan']
                    profile.mobile = data['mobile']
                    profile.clinicsHandling = data['clinicsHandling']
                    profile.firstEmergencyContactNo = data['firstEmergencyContactNo']
                    profile.secondEmergencyContactNo = data['secondEmergencyContactNo']
                    profile.occupation = data['occupation']
                    profile.qualification = data['qualification']
                    profile.specialization = data['specialization']
                    profile.address = data['address']
                    profile.pincode = data['pincode']
                    profile.state = data['state']
                    profile.city = data['city']
                    profile.displayPicture = data['displayPicture']
                    profile.save()
                    return Response({"success":"profile created successfully"})
                except:
                    return Response({"error":"kindly enter all the details"})
            if not created:
                return Response({"error":"mobile number already exists"})


class PaymentOrder(APIView):
    # permission_classes = (permissions.is_authenticated , )
    def post(self,request,format=None):
        data = request.data
        user = User.objects.get(pk=int(data['user']))
        pay = Payment()
        pay.user = user
        clinic_obj = clinic.objects.get(pk=int(data['clinic']))
        pay.clinic = clinic.objects.get(pk=int(data['clinic']))
        pay.subscription = data['plan']
        client = razorpay.Client(auth=("rzp_test_qlBHML4RDDiVon", "DXDDUiaXF06VHM2n4HTdJyQ9"))
        client.set_app_details({"title" : "Django", "version" : "3.0.5"})
        letters = string.ascii_letters
        receipt =  ( ''.join(random.choice(letters) for i in range(10)))
        if 'plan' in data:
            if data['plan'] == 'Monthly':
                DATA = {
                    "amount":599*100,
                    "currency":'INR',
                    "receipt":receipt,
                    "payment_capture":1,
                }
                start_date = datetime.date.today()
                days = datetime.timedelta(days=28)
                end_date = start_date + days
            if data['plan'] == 'Quarterly':
                DATA = {
                    "amount":1699*100,
                    "currency":'INR',
                    "receipt":receipt,
                    "payment_capture":1,
                }
                start_date = datetime.date.today()
                days = datetime.timedelta(days=90)
                end_date = start_date + days
            if data['plan'] == 'Halfly':
                DATA = {
                    "amount":3299*100,
                    "currency":'INR',
                    "receipt":receipt,
                    "payment_capture":1,
                }
                start_date = datetime.date.today()
                days = datetime.timedelta(days=180)
                end_date = start_date + days
            if data['plan'] == 'yearly':
                DATA = {
                    "amount":5999*100,
                    "currency":'INR',
                    "receipt":receipt,
                    "payment_capture":1,
                }
                start_date = datetime.date.today()
                days = datetime.timedelta(days=360)
                end_date = start_date + days
        subscription_exists = clinic_obj.clinicsubscriptions.filter(Q(valid_till__gte=end_date)|Q(subscription_date__lte=end_date)).filter(status='Success').order_by('-valid_till')
        if subscription_exists.count() > 0:
            latest_subscription = subscription_exists[0]
            start_date = latest_subscription.valid_till
            end_date = start_date + days
            print(end_date)
        try:
            c=client.order.create(data=DATA)
            print(c)
            pay.order_id = c['id']
            pay.amount = DATA['amount']
            pay.subscription_date = start_date
            pay.valid_till = end_date
            pay.save()
            return Response({"order_id":pay.order_id,"success":"order was created succesfully",'paymentpk':pay.id})
        except:
            return Response({"error":"same error occur while creating the order please try again"},status=500)


class Validate_Payment(APIView):
    def post(self,request,format=None):
        params = request.data
        client = razorpay.Client(auth=("rzp_test_qlBHML4RDDiVon", "DXDDUiaXF06VHM2n4HTdJyQ9"))
        client.set_app_details({"title" : "Django", "version" : "3.0.5"})
        if 'error' not in params:
            if 'razorpay_order_id' in params:
                try:
                    c=client.order.fetch(params['razorpay_order_id'])
                    params_dict = {
                        'razorpay_order_id':params['razorpay_order_id'],
                        'razorpay_payment_id':params['razorpay_payment_id'],
                        'razorpay_signature':params['razorpay_signature'],
                    }
                    client.utility.verify_payment_signature(params_dict)
                    if c['amount_due'] == 0:
                        p = Payment.objects.get(order_id=params['razorpay_order_id'])
                        p.payment_id = params['razorpay_payment_id']
                        p.status = 'Success'
                        p.save()
                    return Response({"success":"your plan is activated now"})
                except:
                    return Response({"failed":"payment failed try again"},status=500)
        if 'error' in params:
            return Response({"failed":"something went wrong please try again"},status=500)

class Refund_Payment(APIView):
    def post(self,request,format=None):
        data = request.data
        if 'payment' in data:
            pass

@csrf_exempt
def UserRegister(request):
    data = request.POST
    print(data)
    if 'mobile' in data:
        user,c = User.objects.get_or_create(username=data['mobile'])
        if c:
            if 'first_name' in data and 'last_name' in data:
                user.first_name = data['first_name']
                user.last_name = data['last_name']
                user.set_password(data['password'])
                if 'email' in data:
                    user.email = data['email']
                user.save()
                profile = user.profile
                profile.mobile = data['mobile']
                profile.dob = data['dob']
                profile.blood_group = data['blood_group']
                profile.occupation = 'Customer'
                profile.save()
                return JsonResponse({"success":"user created succesfully"},status=200)
        if not c:
            return JsonResponse({"failed":"user already exists try your mobile number as a password"},status=500)
    return JsonResponse({"failed":"please enter the required info"})
