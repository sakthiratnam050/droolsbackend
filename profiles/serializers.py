from django.contrib.auth.models import User , Group
from django.contrib.auth import authenticate
from rest_framework import serializers
from rest_framework.exceptions import *
from .models import *
from rest_framework.response import Response
from django.utils.timesince import timesince
from datetime import  tzinfo
import datetime
from django.contrib.auth.models import User , Group

class UserLiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username','first_name','last_name','id']


class ProfileSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    user = UserLiteSerializer(read_only = True, many = False)
    workingclinics = serializers.SerializerMethodField()
    recopinistclinics = serializers.SerializerMethodField()
    age = serializers.SerializerMethodField()
    class Meta:
        model = profile
        fields = '__all__'
    def get_name(self,obj):
        return obj.user.first_name + " " + obj.user.last_name

    def get_workingclinics(self,obj):
        if obj.occupation is not None:
            if obj.occupation == 'Doctor':
                print("fajkwehfawe")
                working_clinics = obj.user.doctorclinics.prefetch_related('clinic').all()
                data = [{"clinicname":clinic.clinic.companyName,"lat":clinic.clinic.lat,"long":clinic.clinic.long,'fromTime':clinic.fromTimeStr,'toTime':clinic.toTimeStr,'clinicpk':clinic.clinic.pk} for clinic in working_clinics.only('clinic','fromTimeStr','toTimeStr')]
                return data
        return None
    def get_recopinistclinics(self,obj):
        if obj.occupation is not None:
            if obj.occupation == 'MedicalRecoptionist' or obj.occupation == 'ClinicRecoptionist':
                print("fajkwehfawe")
                working_clinics = obj.user.clinicrecopnists.select_related('clinic').all()
                data = [{"clinicname":clinic.clinic.companyName,"lat":clinic.clinic.lat,"long":clinic.clinic.long,"clinicpk":clinic.clinic.id} for clinic in working_clinics.only('clinic')]
                return data
        return None
    def get_age(self,obj):
        if obj.dob is not None:
            year = 365.2425
            start_date = obj.dob
            end_date = datetime.date.today()
            age = round((end_date - start_date).days // year)
            return age
        return "dob not specified"
