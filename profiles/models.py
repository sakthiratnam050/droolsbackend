from django.db import models
from django.contrib.auth.models import User
from time import time
from django.utils import timezone
import datetime
from django.contrib import admin
from django.db.models.signals import post_save , pre_delete , pre_save , post_delete
from django.dispatch import receiver
import requests
import random
import string
import datetime
# from prescriptionapp.models import clinic
# Create your models here.
def getDisplayPicturePath(instance , filename):
    toReturn = 'HR/images/DP/%s_%s_%s' % (str(time()).replace('.', '_'), instance.user.username, filename)
    return toReturn



OCCUPATION_CHOICES = (
    ('Doctor', 'Doctor'),
    ('MediacalRep', 'MediacalRep'),#medicalowner
    ('Customer', 'Customer'),#user
    ('DoctorAssitant', 'DoctorAssitant'),
    ('Others', 'Others'),
    ('Clinic', 'Clinic'),
    ('MedicalRecoptionist', 'MedicalRecoptionist'),#formedical
    ('ClinicRecoptionist', 'ClinicRecoptionist')#forclinic
)

LANGUAGE_CHOICES = (
    ('ENGLISH', 'en'),
    ('HINDI', 'hi')
)
KEY_CHOICES = (
    ('ENGLISH', 'en'),
    ('HINDI', 'hi')
)

class accountsKey(models.Model):
    user = models.ForeignKey(User , related_name='accountKey',on_delete=models.CASCADE)
    activation_key = models.CharField(max_length=40, blank=True)
    key_expires = models.DateTimeField(default=timezone.now)
    keyType = models.CharField(max_length = 15 , default = 'hashed' , choices = KEY_CHOICES)


users = (
    ('Customer','Customer'),
    ('Cook','Cook'),
    ('Admin','Admin'),
)

class profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    displayPicture = models.ImageField(upload_to = getDisplayPicturePath, null = True)
    dob = models.DateField(null=True,blank=True)
    mobile = models.CharField(null = True , max_length = 14, blank = True)
    pan = models.CharField(null = True , max_length = 25, blank = True)
    notificationId = models.CharField(null = True , max_length = 250, blank = True)
    address = models.CharField(null = True , max_length = 200, blank = True)
    pincode = models.CharField(null = True , max_length = 10, blank = True)
    state = models.CharField(null = True , max_length = 10, blank = True)
    city = models.CharField(null = True , max_length = 10, blank = True)
    firstEmergencyContactNo = models.CharField(null = True , max_length = 14, blank = True)
    secondEmergencyContactNo = models.CharField(max_length = 14 , null = True , blank = True)
    occupation = models.CharField(null = True, max_length = 250 , choices = users ,default='Customer')
    specialization =  models.CharField(null = True , max_length = 50, blank = True)
    qualification = models.CharField(null = True , max_length = 50, blank = True)
    uniqueid = models.CharField(null = True,max_length=200, blank = True)
    referralCode = models.CharField(null = True,max_length=300,blank=True)
    walletCreated = models.BooleanField(default = False)
    blood_group = models.CharField(null=True,max_length=300,blank=True)
    isDpVerified = models.BooleanField(default=False)

    @property
    def age(self):
        if self.dob is not None:
            year = 365.2425
            start_date = self.dob
            end_date = datetime.date.today()
            age = round((end_date - start_date).days // year)
            return age
        return "dob not specified"




User.profile = property(lambda u : profile.objects.get_or_create(user = u)[0])


class wallet(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    total_coins = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now=True)



@receiver(post_save, sender=profile, dispatch_uid="server_post_save")
def walletCreation(sender, instance, **kwargs):
    if not instance.walletCreated:
            user = instance.user
            profile = instance.user.profile
            if profile.occupation == 'DOCTOR':
                w,created = wallet.objects.get_or_create(user=user)
                if created:
                    w.total_coins = 100
                    w.save()
            pass

payment_choices  = (
    ('Pending','Pending'),
    ('Success','Success'),
    ('Failed','Failed'),
)

payment_modes  = (
    ('upi','upi'),
    ('cash','cash'),
    ('card','card'),
)

subscription = (
    ('Monthly','Monthly'),
    ('Quarterly','Quarterly'),
    ('Yearly','Yearly'),
    ('Halfly','Halfly'),
)
class Payment(models.Model):
    status = models.CharField(max_length = 200 , null=True,choices=payment_choices,default='Pending')
    subscription = models.CharField(max_length = 200 , null=True,choices=subscription,default='Monthly')
    order_id = models.CharField(max_length = 300 , null=True,blank=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True,related_name='subscriptions')
    payment_mode = models.CharField(max_length = 200 , null=True,choices=payment_modes,default='upi')
    is_received = models.BooleanField(default=False)
    payment_id = models.CharField(max_length=300,null=True,blank=True)
    amount = models.PositiveIntegerField(default=0)
    subscription_date = models.DateField(null=True,blank=True)
    valid_till = models.DateField(null=True,blank=True)
    is_cash = models.BooleanField(default=False)
    # clinic = models.ForeignKey(clinic,on_delete=models.CASCADE,null=True,blank=True,related_name='clinicsubscriptions')
