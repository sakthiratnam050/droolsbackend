from django.contrib import admin
from django.urls import path,include
from rest_framework import routers
from .views import *
router = routers.DefaultRouter()
router.register(r'userss' , ProfileViewSet , basename = 'users')
urlpatterns = [
    path('',include(router.urls)),
    path('createUser/',CreateUsers.as_view()),
    path('paymentOrder/',PaymentOrder.as_view()),
    path('validatePayment/',Validate_Payment.as_view()),
    path('userRegister/',UserRegister),
    path('login/',loginView),

]
